package au.org.uptick.mygwtpapp.server.rpc;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import com.google.inject.Inject;
import com.google.inject.Provider;

import com.gwtplatform.dispatch.server.ExecutionContext;
import com.gwtplatform.dispatch.server.actionhandler.ActionHandler;
import com.gwtplatform.dispatch.shared.ActionException;

import au.org.uptick.mygwtpapp.shared.FieldVerifier;
import au.org.uptick.mygwtpapp.shared.rpc.SendTextToServer;
import au.org.uptick.mygwtpapp.shared.rpc.SendTextToServerResult;

// action handler
// SendTextToServer is action request
// SendTextToServerResult is action result
public class SendTextToServerHandler implements ActionHandler<SendTextToServer, SendTextToServerResult> {

    // use to get http request if it is necessary
    private Provider<HttpServletRequest> requestProvider;
    // use to get servlet context if it is necessary
    private ServletContext servletContext;

    @Inject
    SendTextToServerHandler(ServletContext servletContext, Provider<HttpServletRequest> requestProvider) {
        this.servletContext = servletContext;
        this.requestProvider = requestProvider;
    }

    // execute action and setup action response
    // action response is HTML in this case
    @Override
    public SendTextToServerResult execute(SendTextToServer action, ExecutionContext context) throws ActionException {

        String input = action.getTextToServer();

        if (!FieldVerifier.isValidName(input)) {
            throw new ActionException("Name must be at least 4 characters long");
        }

        String serverInfo = servletContext.getServerInfo();
        String userAgent = requestProvider.get().getHeader("User-Agent");
        return new SendTextToServerResult("Hello, " + input + "!<br><br>I am running " + serverInfo + ".<br><br>It looks like you are using:<br>" + userAgent);
    }

    @Override
    public Class<SendTextToServer> getActionType() {
        return SendTextToServer.class;
    }

    @Override
    public void undo(SendTextToServer action, SendTextToServerResult result, ExecutionContext context) throws ActionException {
    }
}
