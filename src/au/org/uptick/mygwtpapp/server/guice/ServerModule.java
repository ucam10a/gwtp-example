package au.org.uptick.mygwtpapp.server.guice;

import com.gwtplatform.dispatch.server.guice.HandlerModule;

import au.org.uptick.mygwtpapp.server.rpc.SendTextToServerHandler;
import au.org.uptick.mygwtpapp.shared.rpc.SendTextToServer;

// to wrap action request and action handler
public class ServerModule extends HandlerModule {

    // SendTextToServer is action request
    // SendTextToServerHandler is action handler
    @Override
    protected void configureHandlers() {
    
        // now we just bind one handler
        bindHandler(SendTextToServer.class, SendTextToServerHandler.class);
    
    }
    
}
