package au.org.uptick.mygwtpapp.server.guice;

import com.google.inject.servlet.ServletModule;

import com.gwtplatform.dispatch.server.DispatchServiceImpl;
import com.gwtplatform.dispatch.shared.ActionImpl;

// another way to deal with dispatch like presenter
public class DispatchServletModule extends ServletModule {

    @Override
    public void configureServlets() {
        // configure url "/mygwtpapp/dispatch/" to dispatch
        // url can be mutiple string like 
        // serve("/mygwtpapp/" + ActionImpl.DEFAULT_SERVICE_NAME, "rest/").with(DispatchServiceImpl.class);
        serve("/mygwtpapp/" + ActionImpl.DEFAULT_SERVICE_NAME).with(DispatchServiceImpl.class);
    }
    
}
