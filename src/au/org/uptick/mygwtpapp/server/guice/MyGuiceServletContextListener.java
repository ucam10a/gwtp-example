package au.org.uptick.mygwtpapp.server.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

// It is a context listener for binding dispatcher module and action handler module
public class MyGuiceServletContextListener extends GuiceServletContextListener {

    // ServerModule is handler module
    // DispatchServletModule is dispatcher module
    // we can inject more module
    @Override
    protected Injector getInjector() {
        return Guice.createInjector(new ServerModule(), new DispatchServletModule());
    }
    
}
