package au.org.uptick.mygwtpapp.client.view;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import com.google.inject.Inject;

import com.gwtplatform.mvp.client.ViewImpl;

import au.org.uptick.mygwtpapp.client.presenter.MainPagePresenter;

public class MainPageView extends ViewImpl implements MainPagePresenter.MyView {

    // define an empty view
    private static String html = "<h1>My GWTP Starter Project</h1>\n"
            + "<table align=\"center\">\n"
            + "  <tr>\n"
            + "    <td colspan=\"2\" style=\"font-weight:bold;\">Please enter your name:</td>\n"
            + "  </tr>\n"
            + "  <tr>\n"
            + "    <td id=\"nameFieldContainer\"></td>\n"
            + "    <td id=\"sendButtonContainer\"></td>\n"
            + "  </tr>\n"
            + "  <tr>\n"
            + "    <td colspan=\"2\" style=\"color:red;\" id=\"errorLabelContainer\"></td>\n"
            + "  </tr>\n" 
            + "</table>\n";
    
    // as a widget
    HTMLPanel panel = new HTMLPanel(html);

    private final TextBox nameField;
    private final Button sendButton;
    private final Label errorLabel;

    @Inject
    public MainPageView() {
        nameField = new TextBox();
        sendButton = new Button("Send");
        errorLabel = new Label();
        // default name field
        nameField.setText("GWTP User");
        // add all HTML tags to panel
        panel.add(nameField, "nameFieldContainer");
        panel.add(sendButton, "sendButtonContainer");
        panel.add(errorLabel, "errorLabelContainer");
    }

    @Override
    public Widget asWidget() {
        return panel;
    }

    @Override
    public String getName() {
        return nameField.getText();
    }

    @Override
    public Button getSendButton() {
        return sendButton;
    }

    @Override
    public void resetAndFocus() {
        nameField.setFocus(true);
        nameField.selectAll();
    }

    @Override
    public void setError(String errorText) {
        errorLabel.setText(errorText);
    }
}
