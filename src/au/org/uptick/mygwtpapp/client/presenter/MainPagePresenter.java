package au.org.uptick.mygwtpapp.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;

import com.google.inject.Inject;

import com.gwtplatform.mvp.client.EventBus;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.Place;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.client.proxy.RevealRootContentEvent;

import au.org.uptick.mygwtpapp.shared.FieldVerifier;

public class MainPagePresenter extends Presenter<MainPagePresenter.MyView, MainPagePresenter.MyProxy> {

    //action token to call this presenter
    public static final String nameToken = "main";

    private final PlaceManager placeManager;

    //user standard proxy, it means that there is no dispatcher
    //therefore, only one result can be seen
    @ProxyStandard
    @NameToken(nameToken)
    public interface MyProxy extends Proxy<MainPagePresenter>, Place {
    }

    //define view
    public interface MyView extends View {
        String getName();
        Button getSendButton();
        void resetAndFocus();
        void setError(String errorText);
    }

    @Inject
    public MainPagePresenter(EventBus eventBus, MyView view, MyProxy proxy, PlaceManager placeManager) {
        super(eventBus, view, proxy);
        this.placeManager = placeManager;
    }

    //register more handler
    @Override
    protected void onBind() {
        super.onBind();
        // register send button handler
        registerHandler(getView().getSendButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                sendNameToServer();
            }
        }));
    }

    //add more action on reset
    @Override
    protected void onReset() {
        super.onReset();
        // new action
        getView().resetAndFocus();
    }

    //this method must be defined to decide whether to show in parent view
    //if you don't want to show on parent then make it empty
    @Override
    protected void revealInParent() {
        RevealRootContentEvent.fire(this, this);
    }

    // private method for send button click
    private void sendNameToServer() {
        getView().setError("");
        String textToServer = getView().getName();
        // to validate parameter
        if (FieldVerifier.isValidName(textToServer)) {
            // the ResponsePresenter, will do the server call
            placeManager.revealPlace(new PlaceRequest(ResponsePresenter.nameToken).with(ResponsePresenter.textToServerParam, textToServer));
        } else {
            getView().setError("Please enter at least four characters");
        }
    }
}
