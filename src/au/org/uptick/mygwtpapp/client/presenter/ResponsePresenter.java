package au.org.uptick.mygwtpapp.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;

import com.google.inject.Inject;

import com.gwtplatform.dispatch.client.DispatchAsync;
import com.gwtplatform.mvp.client.EventBus;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.Place;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.client.proxy.RevealRootContentEvent;

import au.org.uptick.mygwtpapp.shared.rpc.SendTextToServer;
import au.org.uptick.mygwtpapp.shared.rpc.SendTextToServerResult;

public class ResponsePresenter extends Presenter<ResponsePresenter.MyView, ResponsePresenter.MyProxy> {

    //action token to call this presenter
    public static final String nameToken = "response";
    
    //textToServer parameter name
    public static final String textToServerParam = "textToServer";
    
    //textToServer parameter value
    private String textToServer;

    private final PlaceManager placeManager;
    
    private final DispatchAsync dispatcher;

    // use code split proxy, it means that there are some dispatchers
    //therefore, different results will be seen base on condition
    @ProxyCodeSplit
    @NameToken(nameToken)
    public interface MyProxy extends Proxy<ResponsePresenter>, Place {
    }

    // define view
    public interface MyView extends View {
        Button getCloseButton();
        void setServerResponse(String serverResponse);
        void setTextToServer(String textToServer);
    }

    @Inject
    public ResponsePresenter(EventBus eventBus, MyView view, MyProxy proxy, PlaceManager placeManager, DispatchAsync dispatcher) {
        super(eventBus, view, proxy);
        this.placeManager = placeManager;
        this.dispatcher = dispatcher;
    }

    // for presenter to extract parameters
    @Override
    public void prepareFromRequest(PlaceRequest request) {
        super.prepareFromRequest(request);
        textToServer = request.getParameter(textToServerParam, null);
    }

    // register more handler
    @Override
    protected void onBind() {
        super.onBind();
        // register close button
        registerHandler(getView().getCloseButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                placeManager.revealPlace(new PlaceRequest(MainPagePresenter.nameToken));
            }
        }));
    }

    // add more action on reset
    // because this presenter receive parameter and respond 
    // therefore, reset is equal to response
    // dispatcher has two conditions, one is success the other is failure
    @Override
    protected void onReset() {
        super.onReset();
        getView().setTextToServer(textToServer);
        getView().setServerResponse("Waiting for response...");
        dispatcher.execute(new SendTextToServer(textToServer), new AsyncCallback<SendTextToServerResult>() {
            @Override
            public void onFailure(Throwable caught) {
                getView().setServerResponse("An error occured: " + caught.getMessage());
            }

            @Override
            public void onSuccess(SendTextToServerResult result) {
                getView().setServerResponse(result.getResponse());
            }
        });
    }

    //this method must be defined to decide whether to show in parent view
    //if you don't want to show on parent then make it empty
    @Override
    protected void revealInParent() {
        RevealRootContentEvent.fire(this, this);
    }
}
