package au.org.uptick.mygwtpapp.client;

import au.org.uptick.mygwtpapp.client.gin.MyGinjector;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;

import com.gwtplatform.mvp.client.DelayedBindRegistry;

public class MyGwtpApp implements EntryPoint {

    public final MyGinjector ginjector = GWT.create(MyGinjector.class);

    @Override
    public void onModuleLoad() {

        DelayedBindRegistry.bind(ginjector);
        ginjector.getPlaceManager().revealCurrentPlace();
    
    }
}
