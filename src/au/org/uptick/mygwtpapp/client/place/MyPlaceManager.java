package au.org.uptick.mygwtpapp.client.place;

import au.org.uptick.mygwtpapp.client.presenter.MainPagePresenter;

import com.google.inject.Inject;

import com.gwtplatform.mvp.client.EventBus;
import com.gwtplatform.mvp.client.proxy.PlaceManagerImpl;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.TokenFormatter;

/**
 * use PlaceManagerImpl but you have to define constructor and implement default place
 * this place manager is used to send nameToken and request parameter
 * 
 * @author long
 *
 */
public class MyPlaceManager extends PlaceManagerImpl {

    @Inject
    public MyPlaceManager(EventBus eventBus, TokenFormatter tokenFormatter) {
        super(eventBus, tokenFormatter);
    }

    @Override
    public void revealDefaultPlace() {
        // "main"
        revealPlace(new PlaceRequest(MainPagePresenter.nameToken));
    }
    
}
