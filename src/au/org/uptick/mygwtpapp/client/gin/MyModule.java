package au.org.uptick.mygwtpapp.client.gin;

import au.org.uptick.mygwtpapp.client.place.MyPlaceManager;
import au.org.uptick.mygwtpapp.client.presenter.MainPagePresenter;
import au.org.uptick.mygwtpapp.client.presenter.ResponsePresenter;
import au.org.uptick.mygwtpapp.client.view.MainPageView;
import au.org.uptick.mygwtpapp.client.view.ResponseView;

import com.google.inject.Singleton;

import com.gwtplatform.mvp.client.DefaultEventBus;
import com.gwtplatform.mvp.client.DefaultProxyFailureHandler;
import com.gwtplatform.mvp.client.EventBus;
import com.gwtplatform.mvp.client.RootPresenter;
import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.gwtplatform.mvp.client.proxy.ParameterTokenFormatter;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyFailureHandler;
import com.gwtplatform.mvp.client.proxy.TokenFormatter;

public class MyModule extends AbstractPresenterModule {

    @Override
    protected void configure() {

        // use gwtp class
        bind(EventBus.class).to(DefaultEventBus.class).in(Singleton.class);
        bind(TokenFormatter.class).to(ParameterTokenFormatter.class).in(Singleton.class);
        bind(RootPresenter.class).asEagerSingleton();
        bind(ProxyFailureHandler.class).to(DefaultProxyFailureHandler.class).in(Singleton.class);

        // use 
        bind(PlaceManager.class).to(MyPlaceManager.class).in(Singleton.class);        
        
        // Presenters
        bindPresenter(MainPagePresenter.class, MainPagePresenter.MyView.class, MainPageView.class, MainPagePresenter.MyProxy.class);
        bindPresenter(ResponsePresenter.class, ResponsePresenter.MyView.class, ResponseView.class, ResponsePresenter.MyProxy.class);
    
    }
}
