package au.org.uptick.mygwtpapp.client.gin;

import au.org.uptick.mygwtpapp.client.presenter.MainPagePresenter;
import au.org.uptick.mygwtpapp.client.presenter.ResponsePresenter;

import com.google.gwt.inject.client.AsyncProvider;
import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;

import com.google.inject.Provider;

import com.gwtplatform.dispatch.client.gin.DispatchAsyncModule;
import com.gwtplatform.mvp.client.EventBus;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyFailureHandler;

/**
 * For injection
 * 
 * 
 */
@GinModules({ DispatchAsyncModule.class, MyModule.class })
public interface MyGinjector extends Ginjector {

    // decide event where to go
    EventBus getEventBus();

    // for event history 
    PlaceManager getPlaceManager();

    // get main page presenter, 
    Provider<MainPagePresenter> getMainPagePresenter();

    // get response page presenter, if you use dispatcher, AsyncProvider is needed
    AsyncProvider<ResponsePresenter> getResponsePresenter();

    // get proxy failure handler
    ProxyFailureHandler getProxyFailureHandler();
    
}
