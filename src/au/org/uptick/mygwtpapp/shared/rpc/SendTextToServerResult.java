package au.org.uptick.mygwtpapp.shared.rpc;

import com.gwtplatform.dispatch.shared.Result;

/**
 * The result of a {@link SendTextToServer} action.
 */
public class SendTextToServerResult implements Result {

    private String response;

    public SendTextToServerResult(final String response) {
        this.response = response;
    }

    @SuppressWarnings("unused")
    private SendTextToServerResult() {
    }

    public String getResponse() {
        return response;
    }
}
